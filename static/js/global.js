const theme = document.querySelector("#theme");
const displayMode = document.querySelector("#display-mode");
const dmButton = document.querySelector(".dm-button");

displayMode.addEventListener("mouseover", function () {
    dmButton.classList.remove("far");
    dmButton.classList.add("fas");
});

displayMode.addEventListener("mouseout", function () {
    dmButton.classList.remove("fas");
    dmButton.classList.add("far");
});

function themeDark() {
    theme.setAttribute("href", "static/css/theme-dark.css");
    dmButton.removeAttribute("class");
    dmButton.classList.add("fas");
    dmButton.classList.add("fa-sun");
    localStorage.setItem("theme", "dark");
};

function themeLight() {
    theme.setAttribute("href", "static/css/theme-light.css");
    dmButton.removeAttribute("class");
    dmButton.classList.add("fas");
    dmButton.classList.add("fa-moon");
    localStorage.removeItem("theme");
};

displayMode.addEventListener("click", function () {
    if (theme.getAttribute("href") == "static/css/theme-light.css") {
        themeDark();
    } else if (theme.getAttribute("href") == "static/css/theme-dark.css") {
        themeLight();
    };  
});